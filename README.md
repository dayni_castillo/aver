

> * APELLIDOS INTEGRANTES:CASTILLO MONTENEGRO, CHILUISA CANDO, SOLORZANO NARANJO
> * NOMBRES INTEGRANTES:DAYANA NICOLE,BRAYAN DANILO,ALEX EDUARDO
> * NIVEL	V	 
> * PARALELO:B	
> * CARRERA:COMPUTACION
> * FECHA:8/04/2019
> * ASIGNATURA:BASE DE DATOS
> * DOCENTE:DORIS CHICAIZA
> * NRC:4387




1.	TEMA:  

Ciclo de vida del desarrollo de una Base de Datos


2.	OBJETIVO GENERAL.  

	Detallar información general acerca del Ciclo de vida del desarrollo de una Base de Datos, mediante una investigación vía web, para poder tener conocimientos específicos y desarrollar destreza en el campo laboral, para los alumnos del 5to “B” de la Carrera de Computación de la Unidad de Gestión de Tecnologías Espe.  
  
3.	OBJETIVOS ESPECÍFICOS. 

	Conceptualizar Información acerca del Ciclo de vida del desarrollo de una Base de Datos.  
	Identificar cada ciclo que cumple el desarrollo de una Base de Datos.  
	Comprender todos los parámetros previstos para tener un mejor conocimiento al realizar alguna practica ya    sea en el ámbito académico o laboral.  

4.	INTRODUCCION  

El presente trabajo investigativo se refiere Ciclo de vida del desarrollo de una Base de Datos, que se puede definir como el enfoque utilizado para describir y representar las características y relaciones entre los datos, dentro de la base de datos. El Modelo entidad-relación (E/R), es un Modelo de datos compuesto por objetos llamados entidades y relaciones entre ellos; es el modelo utilizado en el proceso de diseño y desarrollo de la Base de datos del Sistema de Gestión.

5.	DESARROLLO  


































7.  CONCLUSIONES    

8.  RECOMENDACIONES  


9.  BIBLIOGRAFÍA   

